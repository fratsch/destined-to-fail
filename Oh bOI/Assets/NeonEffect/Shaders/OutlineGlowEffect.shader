Shader "Unlit/OutlineGlowEffect"
{
    Properties
    {
        [HDR] _OutlineColor ("Outline Color", Color) = (1,1,1,1)
        _OutlineThreshold ("Outline Threshold", float) = 0.1
        _OutlineRange ("Outline Range", float) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "RenderQueue"="Transparent"}
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Back
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
                float4 worldPos : TEXCOORD0;
            };

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = v.normal;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }
            
            float4 _OutlineColor;
            float _OutlineThreshold;
            float _OutlineRange;

            fixed4 frag (v2f i) : SV_Target
            {
                float3 viewDir = _WorldSpaceCameraPos.xyz - i.worldPos.xyz;
                viewDir = normalize(viewDir);
                float VDotN = dot(viewDir, i.normal);

                float alpha = smoothstep(_OutlineThreshold + _OutlineRange, _OutlineThreshold, VDotN);
                
                float4 col = float4(_OutlineColor.xyz, alpha);
                return col;
            }
            ENDCG
        }
    }
}
