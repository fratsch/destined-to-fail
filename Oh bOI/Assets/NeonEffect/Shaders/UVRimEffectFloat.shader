Shader "Unlit/UVRimEffectFloat"
{
    Properties
    {
        [HDR]_RimColor ("Rim Color", Color) = (1,1,1,1)
        _RimSize ("Rim Size", float) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "RenderType"="Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Back
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float4 _RimColor;
            float _RimSize;

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _RimColor;
                if (i.uv.x < _RimSize || i.uv.x > 1.0f - _RimSize || i.uv.y < _RimSize || i.uv.y > 1.0f - _RimSize)
                {
                    col.a = 1.0f;
                }
                else
                {
                    col.a = 0.0f;
                }
                
                return col;
            }
            ENDCG
        }
    }
}
