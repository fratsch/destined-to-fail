using UnityEngine;

public class HoverRotate : MonoBehaviour
{
	[SerializeField]
	private float rotationSpeed;

	[SerializeField]
	private float hoverSpeed;

	[SerializeField]
	private float maxAmplitude;

	private Transform trans;

	private Vector3 startingPosition;

	private void Awake()
	{
		trans = transform;
		startingPosition = trans.position;
	}

	private void Update()
	{
		float amplitude = Mathf.Sin(Time.time * hoverSpeed) * maxAmplitude;
		trans.position = new Vector3(startingPosition.x, startingPosition.y + amplitude, startingPosition.z);

		trans.Rotate(0.0f, rotationSpeed * Time.deltaTime, 0.0f, Space.World);
	}
}
